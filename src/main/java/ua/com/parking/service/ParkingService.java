package ua.com.parking.service;

import ua.com.parking.model.SlotModel;

public interface ParkingService {

    SlotModel createParkingSlot(String slotNumber);

}
