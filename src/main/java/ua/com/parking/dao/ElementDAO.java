package ua.com.parking.dao;

import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface ElementDAO<E> {

    void addElement(E element);
    void updateElement(final ObjectId id, E element);
    List<E> findAll();
    void deleteElement(final ObjectId id);

}
