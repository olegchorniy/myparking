package ua.com.parking.model;

import java.util.Date;

public class ReservationModel {

    private String id;
    private String carNumber;
    private CustomerModel customer;
    private SlotModel slotModel;
    private Date checkinTime;
    private Date checkoutTime;
    private boolean isActive;

    public String getId() {
        return id;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SlotModel getSlotModel() {
        return slotModel;
    }

    public void setSlotModel(SlotModel slotModel) {
        this.slotModel = slotModel;
    }

    public Date getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(Date checkinTime) {
        this.checkinTime = checkinTime;
    }

    public Date getCheckoutTime() {
        return checkoutTime;
    }

    public void setCheckoutTime(Date checkoutTime) {
        this.checkoutTime = checkoutTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
